
foreach ($size in @("45gb", "80gb", "120gb", "150gb")) {
  vagrant box add koffeiniker/centos7-$size centos7-$size.box
}
