
##############################################################################

# These sizes will be created
#
# If you modify the base-image be sure to adapt an
# eventual Vagrantfile as well!
#
$baseimage = "centos/7"
$sizes = @("45GB", "80GB", "120GB", "150GB")



# Logging, simple
#
function Log-Section($info) {
  write-host ""
  write-host -ForegroundColor yellow ("=" * 79)
  write-host -ForegroundColor yellow "="
  write-host -ForegroundColor yellow "=  $info"
  write-host -ForegroundColor yellow "="
}



# Make the VMDK disk-manager more handy ... PS-like
#
Set-Alias -name Manage-VMDK -value "C:/Program Files (x86)/VMware/VMware Workstation/vmware-vdiskmanager.exe"



# At a first glance the following two lines look idiotic.
# Their purpose is to reset the centos/7 box with a fresh one, by means of not
# being cluttered with snapshots from linked clones.
#
# It is recommended to not use the box directly, anymore.
# Later, we may look for eventually existing snapshots and decide
# on the fly.
#
# So clear the area ...
#
Log-Section "Cleanup ..."
vagrant destroy -f
vagrant box remove $baseimage
#
# Any smart-ass might decide this to be "redundant": have you ever taken a
# glance into the VM directory that Vagrant creates? The VMDK is snapped
# a couple of times. In order to get this straight, flush and rebuild!
# You have been warned!



# Create an instance of centos/7 as full clone with growpart already installed
#
# The full clone will turn the VMDK into a preallocated 40GB monster.
# The Vagrantfile will use a provisioner to bring this back to an
# acceptable level.
#
Log-Section "Create the VM and stop it ..."
vagrant up
vagrant halt -f



# Get all VMDKs in the current Vagrant setup
#
$vmdk = (Get-ChildItem .\.vagrant\machines\default\vmware_desktop\*\*.vmdk).FullName
$vmdks = $vmdk.Count

if ($vmdks -eq 0) {
  Write-Host "No VMDK found. Is the VM present?" -foregroundColor Red
  exit 2
}
if ($vmdks -gt 1) {
  Write-Host "Too many VMDK files found ($vmdks). Only exactly one is supported." -foregroundColor Red
  exit 2
}
Write-Host "Found 1 VMDK as expected."



# Loop through the additional sizes
#
for ($i = 0; $i -lt $sizes.Length; $i++) {

  # At this point we found out our target VMDK.
  # The first package run did shut down the VM.
  # We need to change its size now.
  #
  $s = $sizes[$i]
  Log-Section "Resizing VMDK to $s ..."
  Manage-VMDK -x $sizes[$i] $vmdk

  # Time to fire up again
  #
  Log-Section "Booting the VM ..."
  vagrant up

  # Resize the partition and the filesystem
  #
  Log-Section "Resize the partion and filesystem ..."
  vagrant ssh default -c "sudo growpart /dev/sda 1"
  vagrant ssh default -c "sudo xfs_growfs /"
  vagrant ssh default -c "df"

  # Compact the disk image
  #
  Log-Section "Halting the VM ..."
  vagrant halt
  Log-Section "Preparing the VMDK for packaging ..."
  Manage-VMDK -d $vmdk
  Manage-VMDK -k $vmdk

  # So, time to package the box
  #
  $box = "centos7-" + $sizes[$i] + ".box"
  Log-Section "Packaging $box ...."
  vagrant package --output $box
}

Log-Section "Destroying the VM."
vagrant destroy -f

Write-Host "Done." -ForegroundColor blue
