# Super-Sizer #

## The problem ##
Vagrant allows to instantiate so-called "boxes". 
A box is a pre-installed virtual machine image. 
As such, its disk-size is predefiend, usually around 40GB.

For Virtualbox-Boxes, there exists a vagrant-plugin to 
enlarge the disk-size on-the-fly. For VMware Workstation,
rsp. the VMware Desktop Provider, there is no such feature
as far as I know.

## The starting point ##

I do not like Ruby, but writing a plugin to do what I want
would inevitably involve Ruby. I wouldn't do this even if
someone would pay for it. So the solution will not be a
plugin like the one that exists for Vagrant's Virtualbox
provider.


## My solution ##

I decided to simply create resized boxes, derived from an
original box.

My current project is based on a Windows host and CentOS VMs, so I
chose PowerShell for the scripting. I might have gone for
bash as well. To be honest I just wanted to try out 
PowerShell. I learned a lot about it ... first of all that
I do not like it, either.


## The script ##

The script supersizeme.ps1 will take the box centos/7 and
tweak it a bit:

    - The open-vm-tools will be added, so HGFS will be supported
	- The tool to grow the filesystem will be added
	
The VMDK will be resized and then the partition and filesystem
will be grown to fill the additional space.

After each step of growth, the VM will be packaged and made
available as a box.

As it is, the scripts will create boxes in these sizes:

    - 45GB
	- 80GB
	- 120GB
	- 150GB
	
More or different sizes can be configured easily.


## The helpers ##

There are two helper scripts in the repo:

    - add-boxes.ps1
	- kill-boxes.ps1
	
These are just helper to remove the configured boxes from 
Vagrant rsp. to add the to the boxes known by Vagrant.
